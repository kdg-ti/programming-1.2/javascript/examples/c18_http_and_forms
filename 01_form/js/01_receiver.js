import {createDefinitionlistFromObject,createDefinitionlistFromKeyValueArray} from "./htmlUtils.js";

init();
function init(){
    showPageUrl()
    showSearchParams()
}

// Sample receive1: the 'window.location' object

function showPageUrl(){
    // we extract the infromation of interest from window.location
    // and wrap it in a new object
    document.getElementById("location-contents").replaceChildren( createDefinitionlistFromObject({
        "location": location,
        "location.protocol": location.protocol,
        "location.hostname": location.hostname,
        "location.port": location.port,
        "location.pathname": location.pathname,
        "location.search": location.search,
        "url": `${location.protocol}//${location.hostname}:${location.port}${location.pathname}${location.search}`
    }));
}


// Sample receive2: the 'URLSearchParams' class
// URLSerchparam contains an array with an entry for each search parameter

// each parameter entry is a [name,value] array
// name is at position 0 in this array
// value is at position 1 in this array
function showSearchParams() {
    document.getElementById("query-params").replaceChildren(createDefinitionlistFromKeyValueArray(
      new URLSearchParams(location.search)));
}

