import {createElementWithText} from "./htmlUtils.js"

const feedback = document.getElementById("feedback");

init();
function init(){
    addEventListeners();
    fillForm();
    clearError()
}

function addEventListeners(){
    document.getElementById("password-button").addEventListener("click",showPassword);
    document.getElementById("form-example").addEventListener("submit", checkPassword)
}

// Sample form1: the "URLSearchParams" class
function fillForm() {

    // URLSerchparam contains an array with an entry for each search parameter
    const params = new URLSearchParams(location.search);

    // Here, we"ll put the values of the URL search parameters in the actual form fields.
    // form inputs have a name and a value

    // each entry in params is a [name,value] array
    // name is at position 0 in this array
    // value is at position 1 in this array

    for (const param of params) {
        const name = param[0];
        const value = param[1];
        // parameter value is at position 1 in the para entry
        const element = document.querySelector(`[name=${name}]`);
        if (element instanceof HTMLSelectElement) {
            const optionElement = element.querySelector(`option[value=${value}]`);
            optionElement.selected = true;
        } else if (element instanceof HTMLInputElement) {
            if (element.type === "checkbox") {
                element.checked = value === "on";
            } else if (element.type === "radio") {
                const radioElement = document.querySelector(`[name=${name}][value=${value}]`);
                radioElement.checked = true;
            } else if (element.type !== "file") {
                element.value = value;
            }
        }
    }
    // setting text area
    //document.getElementById("reply").textContent="Text content"
    //document.getElementById("reply").value="Something of value"
}

// Sample form2

function showPassword() {
    // you can always access HTML elements by id
    const passwordDiv = document.getElementById("password-message")
    const formElement = document.querySelector("form");
    // We can access FORM elements  by name
    const password = formElement.elements["password"].value;
    passwordDiv.replaceChildren(createElementWithText("div",`Your password is ${password}`))
    // You can also access FORM elements by index. Name is the first input field
    passwordDiv.append(createElementWithText("div",`Please change it, ${formElement.elements[1].value}!`));
}


// Sample form3
// custom javascript validation
// on submit, prevent the event default action if validation fails
function checkPassword(event) {
    if (document.getElementById("password").value.toLowerCase()
      .includes(document.getElementById("name").value.toLowerCase())){
        setError("No 'John' password, John!");
        event.preventDefault();
    } else{
        clearError();
    }
}

function setError(message) {
    feedback.textContent = message;
    feedback.style.display = "block";
}

function clearError() {
    feedback.textContent = "";
    feedback.style.display = "none";
}