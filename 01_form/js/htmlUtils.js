export function createElementWithText(tag, text) {
  const element = document.createElement(tag);
  element.append(document.createTextNode(text));
  return element;
}

export function createDefinitionlistFromObject(anObject) {
  // Object.entries converts an object to an array with an entry for each attribute
  // each entry  is a [attributename,attributevalue] array
  // name is at position 0 in this array
 // value is at position 1 in this array
  return createDefinitionlistFromKeyValueArray(Object.entries(anObject))
}

export function createDefinitionlistFromKeyValueArray(definitions) {
  const dl = document.createElement("dl");
  // each entry in definitions is a [name,value] array
  // name is at position 0 in this array
  // value is at position 1 in this array
  // we're using destrucuting to assign the first element to the key constant
  // and the second elment to the vzlue constant
  for (const [key,value] of definitions) {
    dl.append(createElementWithText("dt",key),createElementWithText("dd",value));
  }
  return dl
}