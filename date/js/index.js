console.log("Today is ",new Date());
let birthDay = new Date(2022, 4, 26); //26th of MAY
// formatting and timezone depend on the environment
console.log("Birthday",birthDay);
console.log("Parse String and format",new Date("26 May 2022").toLocaleDateString("en-uk"));
console.log("Birthday lunch format with time" ,new Date(2022, 4, 26, 12, 0).toLocaleString("en-uk"));
birthDay.setFullYear(birthDay.getFullYear()+1)
console.log("Next birthday",birthDay);
const DAY_TIME = 1000 * 60 * 60 * 24;
console.log("The week after it's ",new Date(birthDay.getTime() + 7*DAY_TIME).toLocaleDateString("en-uk"));