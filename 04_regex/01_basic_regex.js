let text = 'abc xyz qqq';
regex1 = new RegExp('\\babc\\s+','g');
console.log('Matches? ' + (regex1.test(text) ? 'yes' : 'no'));

const regex2 = /\babc\s+/;
console.log('Matches? ' + (regex2.test(text) ? 'yes' : 'no'));

const regex3 = /\babc\s+/g;
for (let match of text.matchAll(regex3)) {
  console.log("Found match: %s (pos:%d)", match[0], match.index);
}

const regex4 = /\b([a-z]{3})\s+/g;
for (let match of text.matchAll(regex4)) {
  console.log('Found match: ' + match[1]);
}



